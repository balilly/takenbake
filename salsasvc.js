var mongoClient = require('mongodb').MongoClient,
    objectId = require('mongodb').ObjectId,
    express = require('express'),
    fs = require('fs'),
    request = require('request'),
    enumerable = require('linq'),
    json2csv = require('json2csv'),
    dateFormat = require('dateformat'),
    app = express(),
    http = require('http'),
    bodyParser = require('body-parser'),
    queue = require('queue'),
    jwt = require('jsonwebtoken'),
    cookieParser = require('cookie-parser'),
    helmet = require('helmet'),
    mongo = null,
    xml2js = require('xml2js'),
    env = 'config',
    server = http.createServer(app),
    config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

var port = config[env].port,
    accessdata = config[env].accessdata,
    serverUrl = config[env].serverUrl,
    url = config[env].mongoUrl

server.listen(port, function () {
    console.log('Timesheets app listening on port 3012!');
});

app.use(helmet())
app.use(cookieParser())
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
    res.header('Access-Control-Allow-Headers', 'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, SitePrefix, orgtype');
    next();
});

mongoClient.connect(url, function (err, db) {
    mongo = db.db("weidb");
    if (!err) {
        console.log("Connected correctly to db.");
    } else console.log("-------------------------> Mongo connection error." + err)
});

updateAccessDB();

function updateAccessDB() {

    fs.readFile( './test.xml', function(err, data) {
        var parseString = require('xml2js').parseString;
        parseString(data, function (err, result) {
            console.dir(result);
        });
    });

    // var cursor = mongo.collection(collectionName).aggregate(qry, { allowDiskUse: true, explain: false }, null);
    // cursor.on("data", function (data, er) {
    //     if(data.Notes){
    //         data.Notes.forEach( function (x) {
    //             if(x.Type == "Approved" || x.Type == "Denied" || x.Type == "Held" || x.Type == "Moved" || x.Type == "Verify"){
    //                 var obj = {}
    //                 obj.User = x.Creator;
    //                 obj.Id = x.CreatorId;
    //                 obj.Timestamp = new Date(x.Created);
    //                 obj.Action = x.Type;
    //                 mongo.collection(collectionName).updateOne({ "_id": data._id }, { $push: { "Actions" : obj } } , function (err, result) {})
    //             }
    //         })
    //     }
    // });
    // cursor.on("error", function (e) {
    //     console.log('error ---------', e)
    // });
    // cursor.on("end", function () {
    // });
}

// app.use("/landing", express.static("front-end"));
